var searchData=
[
  ['inflatemenu',['inflateMenu',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a3b4bfd362b0867b487bb2cc354232e3e',1,'com.example.searchwidget.SearchBar.inflateMenu(int menuResource)'],['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a71f7fc6ed5b0797b263734590ef49a16',1,'com.example.searchwidget.SearchBar.inflateMenu(int menuResource, int icon)']]],
  ['insert',['insert',['../classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#aaef00578bf6faa511015c5d41562393a',1,'com::example::searchwidget::Adapter::DefaultClientSuggestionsAdapter']]],
  ['issearchenabled',['isSearchEnabled',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a101525ef181280109b31396c3175c91c',1,'com::example::searchwidget::SearchBar']]],
  ['isspeechmodeenabled',['isSpeechModeEnabled',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a1bf37ff1703370a852d5573dfffe5daf',1,'com::example::searchwidget::SearchBar']]],
  ['issuggestionsenabled',['isSuggestionsEnabled',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#afc87e89a8864899a626d2dcafb5db5b2',1,'com::example::searchwidget::SearchBar']]],
  ['issuggestionsvisible',['isSuggestionsVisible',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#aab6325bdf7997340d11ba29cbca66a34',1,'com::example::searchwidget::SearchBar']]]
];
