var searchData=
[
  ['getfilter',['getFilter',['../classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a6c6bf3f7e14e1843e2e4108b8fd05476',1,'com::example::searchwidget::Adapter::SuggestionsAdapter']]],
  ['getitemcount',['getItemCount',['../classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#a5e8c48cf900c2d10e0a8d8ded0877cb2',1,'com::example::searchwidget::Adapter::DefaultClientSuggestionsAdapter']]],
  ['getlastsuggestions',['getLastSuggestions',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a29fff6f8fe5011b1854e4fd4a55ea65f',1,'com::example::searchwidget::SearchBar']]],
  ['getmenu',['getMenu',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#af3e813af6c49f88e24d593dfa41037be',1,'com::example::searchwidget::SearchBar']]],
  ['getplaceholdertext',['getPlaceHolderText',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a97ca29bf8c1fe951bec0dee054187108',1,'com::example::searchwidget::SearchBar']]],
  ['getrequestedquery',['getRequestedQuery',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#acfc402536f5e914bba131f30305b1865',1,'com::example::searchwidget::SearchBar']]],
  ['getsingleviewheight',['getSingleViewHeight',['../classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a5a3d0b988d5f843dae2c31c8a7939176',1,'com::example::searchwidget::Adapter::SuggestionsAdapter']]],
  ['gettext',['getText',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a91a9c3d2ffd4729c083325d8e2ce952d',1,'com::example::searchwidget::SearchBar']]]
];
