var annotated_dup =
[
    [ "com", null, [
      [ "example", null, [
        [ "searchwidget", null, [
          [ "Adapter", null, [
            [ "DefaultClientSuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter" ],
            [ "DefaultLocalSuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter.html", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter" ],
            [ "SuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter" ]
          ] ],
          [ "Builder", null, [
            [ "DefaultClientSuggestions", "classcom_1_1example_1_1searchwidget_1_1_builder_1_1_default_client_suggestions.html", "classcom_1_1example_1_1searchwidget_1_1_builder_1_1_default_client_suggestions" ],
            [ "SearchProp", "classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html", "classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop" ]
          ] ],
          [ "Listener", null, [
            [ "CustomRVItemTouchListener", "classcom_1_1example_1_1searchwidget_1_1_listener_1_1_custom_r_v_item_touch_listener.html", "classcom_1_1example_1_1searchwidget_1_1_listener_1_1_custom_r_v_item_touch_listener" ]
          ] ],
          [ "Model", null, [
            [ "ClientSuggestionsModel", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model" ],
            [ "SearchPropModel", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_search_prop_model.html", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_search_prop_model" ]
          ] ],
          [ "View", null, [
            [ "ClientSuggestionsViewHolder", "classcom_1_1example_1_1searchwidget_1_1_view_1_1_client_suggestions_view_holder.html", "classcom_1_1example_1_1searchwidget_1_1_view_1_1_client_suggestions_view_holder" ]
          ] ],
          [ "SearchBar", "classcom_1_1example_1_1searchwidget_1_1_search_bar.html", "classcom_1_1example_1_1searchwidget_1_1_search_bar" ]
        ] ]
      ] ]
    ] ]
];