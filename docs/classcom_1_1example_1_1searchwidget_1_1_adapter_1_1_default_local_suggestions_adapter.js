var classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter =
[
    [ "OnItemViewClickListener", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter_1_1_on_item_view_click_listener.html", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter_1_1_on_item_view_click_listener" ],
    [ "DefaultLocalSuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter.html#a515a9bd7ec88e96ac5044f50a4a6e09b", null ],
    [ "getSingleViewHeight", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter.html#acd105f7f23aaa7028648eefc3e2e7a2c", null ],
    [ "onBindSuggestionHolder", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter.html#a80c988ed502078be63a27c1384ac4277", null ],
    [ "onCreateViewHolder", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter.html#a7be857e05173df11ba505c1e697dc65f", null ],
    [ "setListener", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter.html#a46d8c7a1850c6199da416980d388882c", null ]
];