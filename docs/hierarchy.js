var hierarchy =
[
    [ "Adapter", null, [
      [ "com.example.searchwidget.Adapter.DefaultClientSuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html", null ],
      [ "com.example.searchwidget.Adapter.SuggestionsAdapter< S, V extends RecyclerView.ViewHolder >", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html", null ]
    ] ],
    [ "AnimationListener", null, [
      [ "com.example.searchwidget.SearchBar", "classcom_1_1example_1_1searchwidget_1_1_search_bar.html", null ]
    ] ],
    [ "com.example.searchwidget.Model.ClientSuggestionsModel", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html", null ],
    [ "com.example.searchwidget.Builder.DefaultClientSuggestions", "classcom_1_1example_1_1searchwidget_1_1_builder_1_1_default_client_suggestions.html", null ],
    [ "OnClickListener", null, [
      [ "com.example.searchwidget.SearchBar", "classcom_1_1example_1_1searchwidget_1_1_search_bar.html", null ]
    ] ],
    [ "OnEditorActionListener", null, [
      [ "com.example.searchwidget.SearchBar", "classcom_1_1example_1_1searchwidget_1_1_search_bar.html", null ]
    ] ],
    [ "OnFocusChangeListener", null, [
      [ "com.example.searchwidget.SearchBar", "classcom_1_1example_1_1searchwidget_1_1_search_bar.html", null ]
    ] ],
    [ "OnItemTouchListener", null, [
      [ "com.example.searchwidget.Listener.CustomRVItemTouchListener", "classcom_1_1example_1_1searchwidget_1_1_listener_1_1_custom_r_v_item_touch_listener.html", null ]
    ] ],
    [ "com.example.searchwidget.Adapter.DefaultClientSuggestionsAdapter.OnItemViewClickListener", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter_1_1_on_item_view_click_listener.html", null ],
    [ "com.example.searchwidget.Adapter.SuggestionsAdapter< S, V extends RecyclerView.ViewHolder >.OnItemViewClickListener", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter_1_1_on_item_view_click_listener.html", [
      [ "com.example.searchwidget.SearchBar", "classcom_1_1example_1_1searchwidget_1_1_search_bar.html", null ]
    ] ],
    [ "com.example.searchwidget.Adapter.DefaultLocalSuggestionsAdapter.OnItemViewClickListener", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter_1_1_on_item_view_click_listener.html", null ],
    [ "com.example.searchwidget.SearchBar.OnSearchActionListener", "interfacecom_1_1example_1_1searchwidget_1_1_search_bar_1_1_on_search_action_listener.html", null ],
    [ "com.example.searchwidget.Listener.CustomRVItemTouchListener.RecyclerViewItemClickListener", "interfacecom_1_1example_1_1searchwidget_1_1_listener_1_1_custom_r_v_item_touch_listener_1_1_recycler_view_item_click_listener.html", null ],
    [ "com.example.searchwidget.Builder.SearchProp", "classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html", null ],
    [ "com.example.searchwidget.Model.SearchPropModel", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_search_prop_model.html", null ],
    [ "com.example.searchwidget.Adapter.SuggestionsAdapter< String, DefaultLocalSuggestionsAdapter.SuggestionHolder >", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html", [
      [ "com.example.searchwidget.Adapter.DefaultLocalSuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_local_suggestions_adapter.html", null ]
    ] ],
    [ "com.example.searchwidget.SearchBar.TextChangeListener", "interfacecom_1_1example_1_1searchwidget_1_1_search_bar_1_1_text_change_listener.html", null ],
    [ "ViewHolder", null, [
      [ "com.example.searchwidget.View.ClientSuggestionsViewHolder", "classcom_1_1example_1_1searchwidget_1_1_view_1_1_client_suggestions_view_holder.html", null ]
    ] ],
    [ "Filterable", null, [
      [ "com.example.searchwidget.Adapter.SuggestionsAdapter< S, V extends RecyclerView.ViewHolder >", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html", null ]
    ] ],
    [ "RelativeLayout", null, [
      [ "com.example.searchwidget.SearchBar", "classcom_1_1example_1_1searchwidget_1_1_search_bar.html", null ]
    ] ]
];