var classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter =
[
    [ "OnItemViewClickListener", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter_1_1_on_item_view_click_listener.html", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter_1_1_on_item_view_click_listener" ],
    [ "DefaultClientSuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#af88a6d4d3b52920e0df4c663a716cc26", null ],
    [ "DefaultClientSuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#a866d175729f5a3fa7b18f85b32e06444", null ],
    [ "clear", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#af10d9536432f1588c882272ae34a9f60", null ],
    [ "getItemCount", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#a5e8c48cf900c2d10e0a8d8ded0877cb2", null ],
    [ "insert", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#aaef00578bf6faa511015c5d41562393a", null ],
    [ "onAttachedToRecyclerView", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#a6f9e2bf4850e26c4ecfa1dd4cf9450ed", null ],
    [ "onBindViewHolder", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#a17f37afa9b401ca97c6655e6bbc168f7", null ],
    [ "onCreateViewHolder", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#aa6b7add18323d70c042ff0c16df28548", null ],
    [ "remove", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_default_client_suggestions_adapter.html#aa7bfa7cd44ecb654ae2575ee77b3f6aa", null ]
];