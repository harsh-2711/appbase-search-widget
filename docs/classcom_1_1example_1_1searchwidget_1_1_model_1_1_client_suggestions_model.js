var classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model =
[
    [ "ClientSuggestionsModel", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#a1774820658c7630006e3ba56e7ce633d", null ],
    [ "getCategory", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#a7b556ed89acd06113c38489dcc01bf46", null ],
    [ "getHits", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#ada5f59fa4f9caa233c70cf0864946069", null ],
    [ "getSearchIcon", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#a926e5fbdabb74b43be5949606fd70d6d", null ],
    [ "getText", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#a56a0e7c32712a948756300e9ce78f006", null ],
    [ "getTrendingIcon", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#a4418706c4ce1037fa2ce8e66b2aa6216", null ],
    [ "setCategory", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#a2628cf20dfe57021afaf75404e0fc972", null ],
    [ "setHits", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#a8ff6c0258aa2e24b108db197ee9f7ea4", null ],
    [ "setSearchIcon", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#ae23b7829d00190022146410c279f1e2a", null ],
    [ "setText", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#a32da5177d9a530d5d9479d13e7a6adab", null ],
    [ "setTrendingIcon", "classcom_1_1example_1_1searchwidget_1_1_model_1_1_client_suggestions_model.html#af6ed718231043653fe1a36629b6e99f0", null ]
];