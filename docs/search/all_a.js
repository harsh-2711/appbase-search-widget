var searchData=
[
  ['search',['search',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a3ccff2a33e4f3bba007aa04840265b47',1,'com::example::searchwidget::SearchBar']]],
  ['searchbar',['SearchBar',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html',1,'com::example::searchwidget']]],
  ['searchprop',['SearchProp',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html',1,'com.example.searchwidget.Builder.SearchProp'],['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#ac87a0dbc238f1faed13afaac53d378fe',1,'com.example.searchwidget.Builder.SearchProp.SearchProp()']]],
  ['searchpropmodel',['SearchPropModel',['../classcom_1_1example_1_1searchwidget_1_1_model_1_1_search_prop_model.html',1,'com::example::searchwidget::Model']]],
  ['setaggregationfields',['setAggregationFields',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#ad6ca29c03b10f0857d3841eb6a77455b',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setaggregationname',['setAggregationName',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#aa37a4af5cb5902d08fc142ddb34a75e1',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setaggregrationstate',['setAggregrationState',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#ad3d4c77b58e9dd37025da3871fbddb7a',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setappbaseclient',['setAppbaseClient',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#aeee8a868bca0435ece56efb35e84602d',1,'com.example.searchwidget.SearchBar.setAppbaseClient(String url, String appName, String username, String password, String type)'],['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a4a4eddac44d1c06f0ac24e31dd653a79',1,'com.example.searchwidget.SearchBar.setAppbaseClient(String url, String appName, String username, String password)']]],
  ['setarrowicon',['setArrowIcon',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a38c90dccdf3d37fb78613230997e91a8',1,'com::example::searchwidget::SearchBar']]],
  ['setarrowicontint',['setArrowIconTint',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#aaf5917642f41df0d4836a7ebe1f5205b',1,'com::example::searchwidget::SearchBar']]],
  ['setautosuggest',['setAutoSuggest',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#a6cc9d28e94707f7877b7e6d90e895f57',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setcardviewelevation',['setCardViewElevation',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#af60f6509efc9ca6466b54e46bd3a5709',1,'com::example::searchwidget::SearchBar']]],
  ['setcategories',['setCategories',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_default_client_suggestions.html#a464457e0b882ce2247919d7ccd8d8ecf',1,'com::example::searchwidget::Builder::DefaultClientSuggestions']]],
  ['setcategoryfield',['setCategoryField',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#af270013263a372ca06b52789d9fee752',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setclearicon',['setClearIcon',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a26dd3ac06416bc0d666d2ce792656c05',1,'com::example::searchwidget::SearchBar']]],
  ['setclearicontint',['setClearIconTint',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#aed6f9a5ee6a225ee697304e6a3ed5d5d',1,'com::example::searchwidget::SearchBar']]],
  ['setcustomsuggestionadapter',['setCustomSuggestionAdapter',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#ac3d6b348b32c8129fca764c3cfe9b810',1,'com::example::searchwidget::SearchBar']]],
  ['setdebounce',['setDebounce',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#aa7c531fb2dd27ab7b393b6fdae6fa7ec',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setdefaultsuggestions',['setDefaultSuggestions',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#aa43e923be48f4dc4896abc8217648346',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setdefaultvalue',['setDefaultValue',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#ae5198e7d95338a48b751e8aa441ef138',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setfuzziness',['setFuzziness',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#af8913b039969c4a33109324554ccee5b',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['sethighlight',['setHighlight',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#a60c9e4bf95a16108265d029e94c8e908',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['sethighlightfield',['setHighlightField',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#a6d0206033754f17f2faeedbd46b54e39',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['sethint',['setHint',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a0f7d73658027bd472ea96b299249f48b',1,'com::example::searchwidget::SearchBar']]],
  ['sethitsenabled',['setHitsEnabled',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#ab331be8e2f324624245a41fcb725b23d',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['seticonripplestyle',['setIconRippleStyle',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a2b5871caacebd12411dbc3bdf3d7c5a1',1,'com::example::searchwidget::SearchBar']]],
  ['seticons',['setIcons',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_default_client_suggestions.html#a00968952b70b8b58bd23dfe15cfab2d4',1,'com::example::searchwidget::Builder::DefaultClientSuggestions']]],
  ['setlastsuggestions',['setLastSuggestions',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a36b0bcb2dd221d8cba1b64955a51b597',1,'com::example::searchwidget::SearchBar']]],
  ['setloggingquery',['setLoggingQuery',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#ad7ebfb25a69de44d845b39a130edcbed',1,'com::example::searchwidget::SearchBar']]],
  ['setmaxsuggestioncount',['setMaxSuggestionCount',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a0f4236bdaf89946d9bdc0eb982cb7cce',1,'com::example::searchwidget::SearchBar']]],
  ['setmenuicon',['setMenuIcon',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a06e0a90b2e6483038bb3d28a92a1a05c',1,'com::example::searchwidget::SearchBar']]],
  ['setmenuicontint',['setMenuIconTint',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a14d304a30b0065246ea3319c730f2e5c',1,'com::example::searchwidget::SearchBar']]],
  ['setnavbuttonenabled',['setNavButtonEnabled',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a4ad3a854a853a3f990f493b053ba8297',1,'com::example::searchwidget::SearchBar']]],
  ['setnavicontint',['setNavIconTint',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a9bfe579751743d32b1008418c6787142',1,'com::example::searchwidget::SearchBar']]],
  ['setonsearchactionlistener',['setOnSearchActionListener',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a065921f97e1f2d1d3c496a5670482f28',1,'com::example::searchwidget::SearchBar']]],
  ['setontextchangelistner',['setOnTextChangeListner',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#afe1c0fa7bdb6574a8c22229de073fa2f',1,'com::example::searchwidget::SearchBar']]],
  ['setplaceholdercolor',['setPlaceHolderColor',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#aa27cf7d4a9e11d4b452a7be85c15e9e6',1,'com::example::searchwidget::SearchBar']]],
  ['setplaceholdertext',['setPlaceHolderText',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a27d40730d4d6a78b9ee89a8c33120d13',1,'com::example::searchwidget::SearchBar']]],
  ['setqueryformat',['setQueryFormat',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#a89945b79d0b5eaa7b78c85aa921228e9',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setredirecticon',['setRedirectIcon',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#a99b979a9754c580a0580be8bbfbeac61',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setroundedsearchbarenabled',['setRoundedSearchBarEnabled',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#af2188426a0fd543d09dfcc0042c9cee9',1,'com::example::searchwidget::SearchBar']]],
  ['setsearchicon',['setSearchIcon',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a3f9335e4481de2b5db38735df6b19753',1,'com::example::searchwidget::SearchBar']]],
  ['setsearchicontint',['setSearchIconTint',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#adca3a2ec79965621b5d77a52ecc68c5b',1,'com::example::searchwidget::SearchBar']]],
  ['setsearchimages',['setSearchImages',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_default_client_suggestions.html#a4c32accb4d2c18d2261b18637f3d4055',1,'com::example::searchwidget::Builder::DefaultClientSuggestions']]],
  ['setsearchprop',['setSearchProp',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a4adb569b19b3f944775531927ab378cf',1,'com::example::searchwidget::SearchBar']]],
  ['setsearchresultimage',['setSearchResultImage',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#ad681308b6d31395f5fa9de5cfe26dcc7',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setspeechmode',['setSpeechMode',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#abcb40436e4d424c4f5ac721d5f507412',1,'com::example::searchwidget::SearchBar']]],
  ['setsuggestionsclicklistener',['setSuggestionsClickListener',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a2c7e29edef133c2885ed261f14dcd329',1,'com::example::searchwidget::SearchBar']]],
  ['setsuggestionsenabled',['setSuggestionsEnabled',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#af24e5b6d3487e7ac76d615e5260a0054',1,'com::example::searchwidget::SearchBar']]],
  ['settext',['setText',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a63dbff92b8fbbdb0fbe556055a99978d',1,'com::example::searchwidget::SearchBar']]],
  ['settextcolor',['setTextColor',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#ac6711d0557a28b29ac41b0ed4e489e02',1,'com::example::searchwidget::SearchBar']]],
  ['settexthighlightcolor',['setTextHighlightColor',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a538551052205575951500130dc5ed591',1,'com::example::searchwidget::SearchBar']]],
  ['settexthintcolor',['setTextHintColor',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a70b21b5c03c532cb74f64e16c992f95d',1,'com::example::searchwidget::SearchBar']]],
  ['settopentries',['setTopEntries',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#a50b19f1a37a91d340aa3a74fd4f4aadc',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['setweights',['setWeights',['../classcom_1_1example_1_1searchwidget_1_1_builder_1_1_search_prop.html#a5b1de91451f74cc827ed342ac134f39c',1,'com::example::searchwidget::Builder::SearchProp']]],
  ['startsearch',['startSearch',['../classcom_1_1example_1_1searchwidget_1_1_search_bar.html#a6d035c502b97b7447dad01c06ca6ee74',1,'com::example::searchwidget::SearchBar']]],
  ['suggestionsadapter',['SuggestionsAdapter',['../classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html',1,'com::example::searchwidget::Adapter']]],
  ['suggestionsadapter_3c_20string_2c_20defaultlocalsuggestionsadapter_2esuggestionholder_20_3e',['SuggestionsAdapter&lt; String, DefaultLocalSuggestionsAdapter.SuggestionHolder &gt;',['../classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html',1,'com::example::searchwidget::Adapter']]]
];
