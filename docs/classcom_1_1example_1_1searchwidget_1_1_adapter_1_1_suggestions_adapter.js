var classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter =
[
    [ "OnItemViewClickListener", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter_1_1_on_item_view_click_listener.html", "interfacecom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter_1_1_on_item_view_click_listener" ],
    [ "SuggestionsAdapter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a92b083ad4ce7550e38b2e6ddee3030c0", null ],
    [ "addSuggestion", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#af12531ec3370dfcc4f8438422b0fd10f", null ],
    [ "clearSuggestions", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a823478b466fd8ca97ed7460cb82587b7", null ],
    [ "deleteSuggestion", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a90aec97ce65d587f0f25886734a7b72b", null ],
    [ "getFilter", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a6c6bf3f7e14e1843e2e4108b8fd05476", null ],
    [ "getItemCount", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a06afe896932e36595282c6c065070031", null ],
    [ "getLayoutInflater", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a4f72bbd9aeab7d72de64e70497638b22", null ],
    [ "getListHeight", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#ac07b9f1783ec7ab013e7a1e54a1fccec", null ],
    [ "getMaxSuggestionsCount", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a145691b2a5c3786a90a544c867962a10", null ],
    [ "getSingleViewHeight", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a5a3d0b988d5f843dae2c31c8a7939176", null ],
    [ "getSuggestions", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a873de2d55dd8c3d9927f5f34824a14a0", null ],
    [ "onBindSuggestionHolder", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#aae68cac86510b1b55bd0277902863738", null ],
    [ "onBindViewHolder", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#afffbcfce909b998240bd783e0da60fdf", null ],
    [ "setMaxSuggestionsCount", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a5f94ad5378ffd25ad7e635f8e424be11", null ],
    [ "setSuggestions", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a75f6b420937b32c6f5a2168a0c97a1ab", null ],
    [ "maxSuggestionsCount", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#aca78c1683ffa9dcce425a196dbd1a56f", null ],
    [ "suggestions", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#a566c0b4e96994ab6729b0dace82eb5e3", null ],
    [ "suggestions_clone", "classcom_1_1example_1_1searchwidget_1_1_adapter_1_1_suggestions_adapter.html#aa09681c2e1f04eae3bf7dc2955e177af", null ]
];